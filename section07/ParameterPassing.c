/*
 ============================================================================
 Name        : ParameterPassing.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Parameter Passing in C, Ansi-style
 ============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void swap(int *x, int *y);


int main() {
	int a = 150;
	int b = 200;

	printf("Before swap, value of a: %d and b : %d\n",a,b);

	// Calling a function to swap the value.
	// %a indicates pointer to a ie. address of variable a and &b indicates pointer to b ie. address of variable b.

	swap(&a, &b);

	printf("After swap, value of a: %d and b: %d\n",a,b);
	return 0;
}

void swap(int *x, int *y){
	int temp;
	temp = *x;
	*x = *y;
	*y = temp;
	return;
}




