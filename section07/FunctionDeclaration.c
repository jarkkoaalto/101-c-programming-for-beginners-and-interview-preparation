/*
 ============================================================================
 Name        : FunctionDeclaration.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Function Declaration in C, Ansi-style
 ============================================================================
*/


#include <stdio.h>

// Function declaration can be placed separately and can also be comibined with function definition
void Fun(int, int);

int main() {
		int x = 10;
		int y = 89;

		Fun(x,y);

		// Fun(1990,2019);
	return 0;
}

void Fun(int a, int b){
	if(a<b){
		printf("%d is smaller than %d",a,b);
	}
	else{
		printf("%d is smaller than %d",b,a);
	}
}

