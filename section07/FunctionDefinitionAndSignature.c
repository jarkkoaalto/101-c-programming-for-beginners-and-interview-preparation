/*
 ============================================================================
 Name        : FunctionDefinitionAndSignature.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Function Definition And Signature in C, Ansi-style
 ============================================================================
*/

#include <stdio.h>

int main() {
	Fun(5,7);
	Fun(10,100);
	Fun(33,45);
	Fun(210,2);

	Fun1();

	return 0;
}

// Function parameter list contains two int value
void Fun(int a, int b)
{
	if(a>b)
		printf("%d is larger than %d. \n", a,b);
	else
		printf("%d is larger or equal to %d \n",b,a);
}

// Function parameter list is empty

void Fun1(){
	printf("Fun1() is an empty function.");
}
