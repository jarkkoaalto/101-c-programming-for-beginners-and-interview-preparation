/*
 ============================================================================
 Name        : Strings.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Strings in C, Ansi-style
 ============================================================================
*/

#include <stdio.h>
#include <string.h>
int main() {

	// Inintialize string in C
	char name[7] = {'S','u','b','s','e','t','\0'};
	// OR
	char string[50] = "C Programmming";
	char name1[] = "Dash";

	printf("Name is : %s\n",name);
	printf("Name is : %s\n", strstr(name, "u"));
	printf("Name is : %x\n", strchr(name, 't'));

	printf("Name is : %d\n", strcmp(name, name1));
	printf("Name is : %s\n", strcat(name,name1));

	printf("\nYour string is: %s\n", string);

	printf("\nYour reverse string is : %s\n",strrev(string));

	return 0;
}




