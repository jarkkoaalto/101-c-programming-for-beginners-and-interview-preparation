/*
 ============================================================================
 Name        : StaticStorageClasses.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Static Storage Classes in C, Ansi-style
 ============================================================================

 */

#include <stdio.h>
#include <stdlib.h>

int test(){
	// int var = 0;
	static int var = 0;
	var++;
	return var;
}

int main(void) {
	printf("%d\n", test());
	printf("%d\n", test());
	printf("%d\n", test());
	printf("%d\n", test());
	return 0;
}




