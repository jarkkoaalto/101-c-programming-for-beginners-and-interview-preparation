/*
 ============================================================================
 Name        : RegisterStorageClasses.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Register Storage Classes in C, Ansi-style
 ============================================================================

 Register variables has the same functionality as of local variables.
 The only difference is that, the compiler tries to store a register
 variable inside a register if a free register is available.

 Other wise it stores inside RAM like other variables.
 Why we need register variable? If quick access is required
 for any variable then we can declare it as register variable.
 For example variables in loops used number of times in a
 short span of time.

 An important point we have to remember that we can not get
 the address of a register variable using pointers.
 */

#include <stdio.h>
#include <stdlib.h>


int main(void) {

	int month;
	register int year;

	return 0;
}




