/*
 ============================================================================
 Name        : AutoStorageClasses.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Auto Storage Classes in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>


int main(void) {

	char y = 5;
	auto char x = 15;
    int u = 70;
	double e = y;
	float r = x;

	printf("%d\n", y);
	printf("%d\n", x);
	printf("%f\n", e);
	printf("%f\n", r);

	Print();
	/*
	 * Two int u variables is different
	 * function int u is only valid inside function
	 * and u can take new value main function see e.g
	 */
	printf("Value of U with in function is : %d\n",u);
	return 0;
}

void Print(){
	auto int u = 50;
	printf("Value of U with in function is : %d\n",u);
}


