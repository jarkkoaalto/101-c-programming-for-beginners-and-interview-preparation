/*
 ============================================================================
 Name        : ExternStorageClasses.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Extern Storage Classes in C, Ansi-style
 ============================================================================

extern storage classes

1. A declaration can be done any number of times but definition only once.
2. �extern� keyword is used to extend the visibility of variables/functions().
3. Since functions are visible throughout the program by default. The use of
extern is not needed in function declaration/definition when function is in
same file but it is needed when function is in a separate/external file.
4. When extern is used with a variable, it�s only declared not defined.
5. As an exception, when an extern variable is declared with initialization,
it is taken as the definition of the variable as well.
*/

#include <stdio.h>
#include <stdlib.h>


int main(void) {

	extern int no;
	printf("The value is: %d",no);

	return 0;
}




