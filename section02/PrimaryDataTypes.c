/*
 ============================================================================
 Name        : PrimaryDataTypes.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Primary Data Types in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int a = 1;
    char b ='G';
    double c = 3.14;
    printf("Hi There!\n");

    //printing the variables defined above along with their sizes
    printf("Hello! I am a character. My value is %c and my size is %lu byte.\n", b,sizeof(char));
    //can use sizeof(b) above as well

    printf("Hello! I am an integer. My value is %d and my size is %lu  bytes.\n", a,sizeof(int));
    //can use sizeof(a) above as well

    printf("Hello! I am a double floating point variable. My value is %lf and my size is %lu bytes.\n",c,sizeof(double));
    //can use sizeof(c) above as well

    printf("Bye! See you soon. :)\n");

    return 0;
}


