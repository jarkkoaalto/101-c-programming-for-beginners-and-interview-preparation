/*
 ============================================================================
 Name        : VariableNamingRules.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Variable Naming Rules in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main() {
	int x1, x2,x3,x4; // 1_x = fail
	short int a = 110;
	int aa = 1;
	char b = 'G';
	double c = 3.1415;

	x1 = 22;
	x2 = 33;
	x3 = 8;

	x4 = x1 + x2 / x3;
	printf("sum of x1 (%d) + x2 (%d) / x3 (%d) = %d\n",x1,x2,x3,x4);
	printf("Char value : %c\n", b);
	printf("PI is %f\n", c);
	printf("%d + %d = %d\n", x4,a,(x4+a));

	return 0;
}


