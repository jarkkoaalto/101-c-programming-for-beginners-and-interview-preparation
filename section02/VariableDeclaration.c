/*
 ============================================================================
 Name        : VariableDeclaration.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Variable Declaration in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main() {

	// Declaration and definition of variable 'name'
	char name = 'a';

	// both declaraton and definition occurs. 'roll' is allocated
	// memory and assigned some garbage value.
	float roll;

	// multible declarations and definitions
	int _math, _sc, _eng;

	printf("%c \n",name);


	return 0;
}


