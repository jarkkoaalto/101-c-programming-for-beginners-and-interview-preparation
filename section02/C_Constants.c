/*
 ============================================================================
 Name        : C_Constants.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Constants in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define INCREMENT(x) ++x

int main() {

	const int CODE = 10;
	const float PI = 3.1415;
	printf("Code of Organisation: %d\n", CODE);

	char y =  5;
	char x = 10;
	float r = 12;
	float area = r * r * PI;

	printf("Area is: %f\n", area);
	printf("%d\n", INCREMENT(y));
	printf("%d\n", INCREMENT(x));
	return 0;
}


