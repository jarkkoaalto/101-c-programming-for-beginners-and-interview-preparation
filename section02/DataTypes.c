/*
 ============================================================================
 Name        : DataTypes.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Data types in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main() {
	char str[40];
	printf("Enter a value: ");
	scanf(str);
	printf("\nYou entered: ");
	puts(str);
	return 0;
}


