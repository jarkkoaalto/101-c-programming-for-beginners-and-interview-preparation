/*
 ============================================================================
 Name        : Arrays.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Arrays C, Ansi-style
 ============================================================================


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {
	int student[5] = {31,56,68,34,5};
	printf("Student no: %d\n",student[0]);
	printf("Student no: %d\n",student[1]);
	printf("Student no: %d\n",student[2]);
	printf("Student no: %d\n",student[3]);
	printf("Student no: %d\n",student[4]);

	int arr[4];
	int i,j;
	printf("Enter array element\n");
	for(i=0;i<4;i++){
		scanf("%d\n", &arr[i]);
	}
	for(j=0;j<4;j++){
		printf("%d\n",arr[j]);
	}
	return 0;
}

*/


