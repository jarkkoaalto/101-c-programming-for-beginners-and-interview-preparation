/*
 ============================================================================
 Name        : SwitchStatement.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Switch Statement in C, Ansi-style
 ============================================================================
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



int main() {

	int ch = 1;

	switch(ch){
	case 1:
		printf("Mon");
		break;
	case 2:
		printf("Tue");
		break;
	case 3:
		printf("Wen");
		break;
	case 4:
		printf("Thu");
		break;
	case 5:
		printf("Fri");
		break;
	case 6:
		printf("Sat");
		break;
	case 7:
		printf("Sun");
		break;
	default:
		printf("Only value between 1 to 7");
		break;

	}
	return 0;

}




