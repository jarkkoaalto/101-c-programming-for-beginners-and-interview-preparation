/*
 ============================================================================
 Name        : GotoStatement.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Goto Statement in C, Ansi-style
 ============================================================================
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



int main() {

	int number = 1;

repeat:
	printf("%d\n",number);
	number++;

	if(number <= 10)
		goto repeat;

	return 0;

}




