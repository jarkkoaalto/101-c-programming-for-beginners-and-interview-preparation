/*
 ============================================================================
 Name        : NestedIfStatement.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Nested If Statement in C, Ansi-style
 ============================================================================
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



int main() {

	int a=15, b=16, c=17;
	if(a > b)
	{
		if(a > c)
		{
			printf("a is the greatest");
		}
		else
		{
			printf("c is thet greatest");
		}
	}
	else
	{
		if(b > c)
		{
			printf("b is the greatest");
		}
		else
		{
			printf("c is the greatest");
		}
	}
	return 0;

}




