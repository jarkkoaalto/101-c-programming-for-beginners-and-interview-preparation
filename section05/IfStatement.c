/*
 ============================================================================
 Name        : IfStatement.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : If Statement in C, Ansi-style
 ============================================================================
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



int main(void) {

	int x, y;
	x = 18;
	y = 114;


	//if(x > y)
	//	printf("x is greater than y\n");


	if(x>y)
		printf("x is greater than y");
	else
		printf("x is less than y");

	return 0;

}




