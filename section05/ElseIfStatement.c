/*
 ============================================================================
 Name        : ElseIfStatement.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Else If Statement in C, Ansi-style
 ============================================================================
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



int main() {

	int a=15;
	if(a%3 == 0 && a%5 == 0)
	{
		printf("Disionable by both 3 and 5");
	}
	else if (a%3 == 0)
	{
		printf("Divisible by only 3");
	}
	else if(a%5 == 0)
	{
		printf("Divisible by only 5");
	}
	else
	{
		printf("Divisible ny neither 3 nor 5");
	}
	return 0;

}




