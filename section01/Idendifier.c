/*
 ============================================================================
 Name        : Identifier.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : C identifier in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	puts("This is a statement");
	sub_123Car();
	return 0;
}

/*
 * Rules for an Identifier
	An Identifier can only have alphanumeric characters(a-z , A-Z , 0-9)
	 and underscore(_).

	The first character of an identifier can only contain
	alphabet(a-z , A-Z) or underscore (_).

	Identifiers are case sensitive in C. For example name and
	Name are two different identifiers in C.

	Keywords are not allowed to be used as Identifiers.

	No special characters, such as semicolon, period, whitespaces,
 	slash or comma are permitted to be used in or as Identifier.
 */

void sub_123Car(){ // @$# Is not allowed
	printf("Sub_123Car");
}
