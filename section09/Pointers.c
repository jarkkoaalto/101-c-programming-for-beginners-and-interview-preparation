/*
 ============================================================================
 Name        : Pointers.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Pointers C, Ansi-style
 ============================================================================



#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {
	int var = 10; // Actual variable declaration
	int *xp; // pointer varable declaration

	xp = &var; // strores of var in ponter variable xp

	printf("Address of var variable is : %xp\n", &var);

	printf("Address stored in xp variables is : %xp\n",xp);
	printf("Value of var variable is : %d\n",var);
	printf("Value of *xp variable is : %d\n", *xp);
	return 0;
}

*/

