/*
 ============================================================================
 Name        : Pointers.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Pointers C, Ansi-style
 ============================================================================



#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {

	int var[] = {41,51,61};
	int i, *ptr;

	// lets us have array address in pointer
	ptr = var; //using array name without braces will give the base address of array
	// so no need to add &
	// or you can also write ptr = *var[0]
	for(i=0;i<3;i++){
		printf("Address of var[%d] = %x\n",i,ptr);
		printf("Value of var[%d] = %d\n", i,*ptr);
		ptr++;
	}


	return 0;
}
*/


