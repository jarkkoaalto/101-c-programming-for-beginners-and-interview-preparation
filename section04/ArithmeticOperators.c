/*
 ============================================================================
 Name        : ArithmeticOperators.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : SArithmetic Operators in C, Ansi-style
 ============================================================================

*/

#include <stdio.h>
#include <stdlib.h>



int main(void) {
	int a = 10, b = 4,res;

	// printing a, b value
	printf("a is %d and b is %d\n", a,b);

	res = a+b;
	// printing addition result
	printf("a+b = %d\n",res);

	res = a-b;
	printf("a-b = %d\n",res);

	res = a*b;
	printf("a*b = %d\n",res);

	res = a/b;
	printf("a/b = %d\n",res);

	res = a%b;
	printf("a%b = %d\n",res);


	/*
	 * Post-increment example:
	 * res is assigned 10 only, a is not updated yet
	 *
	 */
	res = a++;
	printf("a is %d and res is %d\n",a,res);

	/*
	 * Post-decrement example: res is assigned 11 only,a is not updated yet
	 */
	res = a--;
	printf("a is %d and res is %d\n",a,res);

	/*
	 * Pre-increment example: res is assigned 11 now since a is updated here itself
	 */
	res = ++a;
	printf("a is %d and res is %d\n", a,res);

	// pre-decrement example: res is assigned 10 only since a is update here itself
	res = --a;
	printf("a is %d and res is %d\n",a,res);
	return 0;
}




