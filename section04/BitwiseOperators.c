/*
 ============================================================================
 Name        : BitwiseOperators.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Bitwise Operators in C, Ansi-style
 ============================================================================

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



int main(void) {

	unsigned char a = 5, b=9;
	printf("a = %d, b = %d\n",a,b);
	printf("a&b = %d\n",a&b);
	printf("a|b = %d\n",a|b);
	printf("~a = %d\n",a = ~a);
	printf("b<<1 = %d\n",b<<1);

	return 0;

}




