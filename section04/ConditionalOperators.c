/*
 ============================================================================
 Name        : ConditionalOperators.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Conditional Operators in C, Ansi-style
 ============================================================================

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



int main(void) {

	int a=5, b=6, c=7,large;
	large = a>b ? (a>c?a:c) : (b>c?b:c);
	printf("The larges number is %d\n", large);


	return 0;

}




