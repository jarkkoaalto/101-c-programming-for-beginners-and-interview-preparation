/*
 ============================================================================
 Name        : LogicalOperators.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Logical Operators in C, Ansi-style
 ============================================================================
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



int main(void) {
	int a = 12, b = 8, c = 12, d = 23;

	if(a>b && c==d)
		printf("a is greater than b AND c is equal to d\n");
	else
		printf("AND conduction is not satisfying\n");

	if(a>b||c==d)
		printf("a is greater than b OR c is equal to d\n");
	else
		printf("Neither a is greater than b nor c is equal to d\n");

	if(!a)
		printf("Value of a is zero\n");
	else
		printf("Value of a is not zero\n");


	int p = 12,f = 14;
	bool res = ((p==f)) && printf("A is less than B");


	bool res1 = ((p != f)) && printf("A is not equal to B");


	return 0;

}




