/*
 ============================================================================
 Name        : DoWhile.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Do While Loop in C, Ansi-style
 ============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {
	int a = 3;
	do{
		printf("Value of a: %d\n",a);
		a++;
	}while(a <= 10);

	return 0;
}




