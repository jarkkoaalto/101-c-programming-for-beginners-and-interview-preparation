/*
 ============================================================================
 Name        : WhileLoop.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : While Loop in C, Ansi-style
 ============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {
	int a;
	for(a=5;a<=10;a++){
		printf("Value of a: %d\n",a);
	}

	return 0;
}




