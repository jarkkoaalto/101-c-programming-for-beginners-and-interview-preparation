/*
 ============================================================================
 Name        : WhileLoop.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : While Loop in C, Ansi-style
 ============================================================================

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



int main() {

	int a = 5;

	while(a < 10){
		printf("Value of a: %d\n",a);
		a++;
	}

	return 0;

}




