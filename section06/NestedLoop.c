/*
 ============================================================================
 Name        : NestedLoop.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Nested Loop in C, Ansi-style
 ============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {
	int i,j, no = 1;

	for(i=1;i<=5;i++){
		for(j=1;j<=5;j++){
			printf("%d ", no++);
		}
		printf("\n");
	}
	return 0;
}




