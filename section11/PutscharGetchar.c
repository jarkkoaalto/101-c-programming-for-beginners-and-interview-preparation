/*
 ============================================================================
 Name        : PutscharGetschar.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Putschar and Getschar in C, Ansi-style
 ============================================================================


#include <stdio.h>
#include <stdlib.h>

int main(void) {
	char ch;
	printf("Enter a character: ");
	ch = getchar();

	printf("The entered character is: ");
	putchar(ch);

	return 0;
}
 */
