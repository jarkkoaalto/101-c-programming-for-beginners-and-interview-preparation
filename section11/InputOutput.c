/*
 ============================================================================
 Name        : InputOutput.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Input Output in C, Ansi-style
 ============================================================================


#include <stdio.h>

int main() {

	char a[100], b[100], c[100];

	// printf with out any format string and variables
	printf("Enter one word and pres enter \n");

	int i = scanf("%s",a);
	printf("\n First scanf() returns: %d with value : %s\n",i,a);

	printf("Enter two words and press enter\n");

	i = scanf("%s%s", a,b);
	printf("\nSecond scanf() returns: %d with values: %s %s \n",i,a,b);

	printf("Enter three words and press Enter\n");

	i = scanf("%s%s%s",a,b,c);
	printf("\nThree scanf() returns: %d with value: %s %s %s\n",i,a,b,c);
	return 0;
}
*/



