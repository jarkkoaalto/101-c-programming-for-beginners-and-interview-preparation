/*
 ============================================================================
 Name        : PutsGets.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Puts and Gets in C, Ansi-style
 ============================================================================


#include <stdio.h>
#include <stdlib.h>

int main(void) {
	char ch[40];
	printf("Enter a value : ");
	gets(ch);

	printf("\nYou entered: ");
	putchar(ch);

	return 0;
}

 */
